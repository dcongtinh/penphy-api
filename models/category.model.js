const mongoose = require('mongoose')

var CategorySchema = mongoose.Schema(
    {
        name: String,
        icon: String
    },
    { timestamps: true }
)

module.exports = mongoose.model('Category', CategorySchema)
