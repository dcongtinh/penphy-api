const mongoose = require('mongoose')

var NotificationSchema = mongoose.Schema(
    {
        sender: {
            type: String,
            ref: 'User'
        },
        receiver: {
            type: String,
            ref: 'User'
        },
        type: String,
        content: String,
        status: String, //ACCEPTED, PENDING, REJECTED, NEW, READ
        course: {
            type: String,
            ref: 'Course'
        }
    },
    { timestamps: true }
)

module.exports = mongoose.model('Notification', NotificationSchema)
