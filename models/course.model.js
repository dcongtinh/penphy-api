const mongoose = require('mongoose')

var CourseSchema = mongoose.Schema(
    {
        name: String,
        description: String,
        content: String,
        category: [
            {
                type: String,
                ref: 'Category'
            }
        ],
        price: Number,
        seats: Number,
        registeredSeats: Number,
        distance: Number,
        cover: String,
        mentor: {
            type: String,
            ref: 'User'
        }
    },
    { timestamps: true }
)

CourseSchema.index({
    name: 'text',
    description: 'text'
})

module.exports = mongoose.model('Course', CourseSchema)
