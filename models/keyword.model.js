const mongoose = require('mongoose')

var KeywordSchema = mongoose.Schema(
    {
        keyword: String
    },
    { timestamps: true }
)

KeywordSchema.index({
    keyword: 'text'
})

module.exports = mongoose.model('KeyWord', KeywordSchema)
