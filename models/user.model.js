const mongoose = require('mongoose')
const bcrypt = require('bcrypt')

var UserSchema = mongoose.Schema(
    {
        username: String,
        password: String,
        fullname: String,
        gender: {},
        age: Number,
        address: String,
        email: String,
        phoneNumber: String,
        achieves: [],
        follow_category: [
            {
                type: String,
                ref: 'Category'
            }
        ],
        medals: [],
        role: String,
        avatar: String,
        cover: String,
        rating: Number,
        registeredCourses: [
            {
                type: String,
                ref: 'Category'
            }
        ]
    },
    { timestamps: true }
)

UserSchema.methods.generateHash = password =>
    bcrypt.hashSync(password, bcrypt.genSaltSync(8), null)
UserSchema.methods.validPassword = function validPassword(password) {
    return bcrypt.compareSync(password, this.password)
}

UserSchema.methods.comparePassword = function comparePassword(
    password,
    callback
) {
    bcrypt.compare(password, this.password, callback)
}

UserSchema.index({
    fullname: 'text',
    email: 'text'
})
module.exports = mongoose.model('User', UserSchema)
