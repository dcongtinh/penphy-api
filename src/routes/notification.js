const express = require('express')
const router = new express.Router()
const jwt = require('jsonwebtoken')

const md5 = require('md5')
const moment = require('moment')

const Notification = require('../../models/notification.model')

router.get('/:id', (req, res) => {
    Notification.find({ _id: req.params.id })
        .populate({ path: 'course', select: 'name description' })
        .exec((err, data) => {
            if (err) return res.json({ error: true, data: err + '' })
            return res.json({ error: false, data })
        })
})

router.post('/', (req, res) => {
    Notification.create(req.body)
    return res.json({ data: req.body })
})

router.put('/:id', (req, res) => {
    Notification.findByIdAndUpdate(
        req.params.id,
        {
            $set: {
                status: req.body.status
            }
        },
        { new: true }
    )
        .populate({ path: 'course' })
        .exec((err, data) => {
            if (err)
                return res.json({
                    error: true,
                    data: err + ''
                })
            return res.json({ error: false, data })
        })
})

router.delete('/:id', (req, res) => {
    Notification.findByIdAndRemove(req.params.id).exec((err, data) => {
        if (err)
            return res.json({
                error: true,
                data: err + ''
            })
        return res.json({ error: false, data })
    })
})

module.exports = router
