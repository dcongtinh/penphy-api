const express = require('express')
const router = new express.Router()
const jwt = require('jsonwebtoken')

const md5 = require('md5')
const moment = require('moment')

const User = require('../../models/user.model')
const Course = require('../../models/course.model')
const Keyword = require('../../models/keyword.model')

router.get('/course', (req, res) => {
    const { q } = req.query
    const perPage = req.query.perPage ? parseInt(req.query.perPage) : 10
    const page = req.query.page ? parseInt(req.query.page) : 1
    const skip = (page - 1) * perPage

    Course.find(
        { $text: { $search: q } },
        { htmlContent: false, rawContent: false, textContent: false }
    )
        .populate({ path: 'category' })
        .populate({ path: 'mentor', select: 'fullname rating avatar' })
        .limit(perPage)
        .skip(skip)
        .exec((err, data) => {
            if (err)
                return res.json({
                    error: true,
                    message: err + ''
                })

            Keyword.create({ keyword: q })
            return res.json({ error: false, data })
        })
})

router.get('/user', (req, res) => {
    const { q } = req.query
    const sort = req.query.sort ? JSON.parse(req.query.sort) : { createdAt: -1 }
    const perPage = req.query.perPage ? parseInt(req.query.perPage) : 10
    const page = req.query.page ? parseInt(req.query.page) : 1
    const skip = (page - 1) * perPage

    User.find(
        { $text: { $search: q } },
        { htmlContent: false, rawContent: false, textContent: false }
    )
        .sort(sort)
        .limit(perPage)
        .skip(skip)
        .exec((err, data) => {
            if (err)
                return res.json({
                    error: true,
                    message: err + ''
                })
            Keyword.create({ keyword: q })
            return res.json({ error: false, data })
        })
})

router.get('/keywords', (req, res) => {
    Keyword.find().exec((err, data) => {
        if (err) return res.json({ error: true, data: err + '' })
        return res.json({ error: false, data })
    })
})

router.get('/keyword', (req, res) => {
    const { q } = req.query

    Keyword.find(
        { $text: { $search: q } },
        { htmlContent: false, rawContent: false, textContent: false }
    ).exec((err, data) => {
        if (err)
            return res.json({
                error: true,
                message: err + ''
            })
        return res.json({
            error: false,
            data
        })
    })
})
module.exports = router
