const express = require('express')
const router = new express.Router()
const jwt = require('jsonwebtoken')

const md5 = require('md5')
const moment = require('moment')

const Course = require('../../models/course.model')

router.get('/', (req, res) => {
    const perPage = req.query.perPage ? parseInt(req.query.perPage) : 10
    const page = req.query.page ? parseInt(req.query.page) : 1
    const skip = (page - 1) * perPage

    Course.find()
        .populate({
            path: 'category'
        })
        .populate({
            path: 'mentor',
            select:
                'fullname avatar rating seats registeredSeats price distance'
        })
        .limit(perPage)
        .skip(skip)
        .exec((err, data) => {
            if (err)
                return res.json({
                    error: true,
                    message: err + ''
                })
            return res.json({ error: false, data })
        })
})

router.post('/', (req, res) => {
    Course.create(req.body)
    return res.json({
        error: false,
        data: req.body
    })
})

router.put('/:id', (req, res) => {
    const payload = {
        name: req.body.name,
        category: req.body.category,
        price: req.body.price,
        seats: req.body.seats,
        registeredSeats: req.body.registeredSeats,
        distance: req.body.distance,
        cover: req.body.cover
    }

    Course.findByIdAndUpdate(
        req.params.id,
        {
            $set: payload
        },
        { new: true }
    ).exec((err, data) => {
        if (err) return res.json({ error: true })

        if (data)
            return res.json({
                error: false,
                data
            })
    })
})

router.delete('/:id', (req, res) => {
    Course.findByIdAndRemove(req.params.id).exec(err => {
        if (err) return res.json({ error: true })
        return res.json({ error: false })
    })
})

module.exports = router
