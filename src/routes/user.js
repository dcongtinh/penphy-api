const express = require('express')
const router = new express.Router()
const jwt = require('jsonwebtoken')

const md5 = require('md5')
const moment = require('moment')

const User = require('../../models/user.model')
const Notification = require('../../models/notification.model')

router.get('/:id', function(req, res) {
    User.find({ _id: req.params.id })
        .populate({ path: 'follow_category' })
        .populate({ path: 'registeredCourses' })
        .exec((err, data) => {
            if (err) return res.json({ error: true, data: err + '' })
            return res.json({ error: false, data })
        })
})

router.post('/login', (req, res) => {
    const payload = {
        email: req.body.email,
        password: md5(req.body.password)
    }

    const token = jwt.sign({ email: payload.email }, 'dcongtinh')
    // res.json(token)
    jwt.verify(token, 'dcongtinh', (err, decoded) => {
        if (err) {
            return res.json({
                error: true,
                data: err + ''
            })
        }
        User.findOne({ email: payload.email }).exec((err, data) => {
            if (err) {
                return res.json({
                    error: true,
                    data: err + ''
                })
            }
            if (data) {
                if (data.password != payload.password)
                    return res.json({
                        error: true,
                        data: 'Sai ten tai khoan hoac mat khau'
                    })
                // Remove item
                delete data.password
                delete data.__v

                // Response
                return res.json({
                    error: false,
                    token,
                    data
                })
            } else {
                return res.json({
                    error: true,
                    data: 'Không tìm thấy thành viên'
                })
            }
        })
    })
})

router.post('/register', (req, res) => {
    const data = {
        password: md5(req.body.password),
        gender: {
            value: 'MALE',
            label: 'Nam'
        },
        fullname: req.body.fullname,
        age: req.body.age,
        address: req.body.address,
        email: req.body.email,
        phoneNumber: req.body.phoneNumber,
        achieves: req.body.achieves,
        follow_category: req.body.follow_category,
        medals: req.body.medals,
        role: req.body.role,
        avatar: req.body.avatar,
        cover: req.body.cover,
        rating: req.body.rating,
        registeredCourses: req.body.registeredCourses
    }

    User.findOne({ email: req.body.email }).exec((err, userData) => {
        if (err) return res.json({ error: true })
        if (userData) {
            return res.json({
                error: true,
                data: 'Tài khoản đã tồn tại'
            })
        }
        User.create(data)
        return res.json({
            error: false,
            data
        })
    })
})

router.put('/:id/profile', (req, res) => {
    const { id } = req.params
    User.findByIdAndUpdate(
        id,
        {
            $set: req.body
        },
        { new: true }
    ).exec((err, data) => {
        if (err) {
            return res.json({
                error: true,
                data: err + ''
            })
        }
        res.json({
            error: false,
            data
        })
    })
})

router.post('/changePassword/:id', (req, res) => {
    const payload = {
        newPassword: req.body.newPassword,
        renewPassword: req.body.renewPassword
    }

    const { newPassword, renewPassword } = payload
    User.findByIdAndUpdate(
        req.params.id,
        {
            $set: {
                password: md5(newPassword)
            }
        },
        { new: true }
    ).exec((err, data) => {
        if (err) return res.json({ error: true })

        if (data) {
            if (newPassword != renewPassword)
                return res.json({ message: 'Mat khau khong khop!!' })
            return res.json({
                error: false,
                password: data.password
            })
        }
    })
})

router.get('/:userID/notifications', (req, res) => {
    const { name } = req.body
    const { userID } = req.params
    Notification.find({ receiver: userID })
        .populate({
            path: 'sender',
            select: 'username avatar'
        })
        .populate({
            path: 'receiver',
            select: 'username avatar'
        })
        .exec((err, data) => {
            if (err) {
                return res.json({
                    error: true,
                    data: err + ''
                })
            }
            res.json({
                error: false,
                data
            })
        })
})

router.put('/:id', (req, res) => {
    User.findByIdAndUpdate(
        req.params.id,
        {
            $set: req.body
        },
        { new: true }
    ).exec((err, data) => {
        if (err) return res.json({ error: true })

        if (data)
            return res.json({
                error: false,
                data
            })
    })
})

router.delete('/:id', (req, res) => {
    User.findByIdAndRemove(req.params.id).exec(err => {
        if (err) return res.json({ error: true, data: err + '' })
        return res.json({ error: false })
    })
})
module.exports = router
