const express = require('express')
const router = new express.Router()
const jwt = require('jsonwebtoken')

const md5 = require('md5')
const moment = require('moment')
const Category = require('../../models/category.model')

router.get('/', (req, res) => {
    Category.find().exec((err, data) => {
        if (err)
            return res.json({
                error: true,
                data: err + ''
            })
        return res.json(data)
    })
})

router.post('/', (req, res) => {
    Category.findOne({ name: req.body.name }).exec((err, data) => {
        if (err) return res.json({ error: true })
        if (data) return res.json({ message: 'Category da ton tai!!!' })
        Category.create(req.body)
        return res.json({
            error: false,
            data: req.body
        })
    })
})

router.put('/:id', (req, res) => {
    Category.findByIdAndUpdate(
        req.params.id,
        {
            $set: req.body,
            $set: {
                updatedAt: new Date()
            }
        },
        { new: true }
    ).exec((err, data) => {
        if (err)
            return res.json({
                error: true,
                data: err + ''
            })
        return res.json({ error: false, data })
    })
})

router.delete('/:id', (req, res) => {
    Category.findByIdAndRemove(req.params.id).exec((err, data) => {
        if (err)
            return res.json({
                error: true,
                data: err + ''
            })
        return res.json({ error: false, data })
    })
})

module.exports = router
