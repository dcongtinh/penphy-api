console.log('Oh Yeah!')
const express = require('express')
const app = express()

const http = require('http')
const socketio = require('socket.io')
const server = http.Server(app)
const websocket = socketio(server)

const bodyParser = require('body-parser')
app.use(bodyParser.json())

const mongoose = require('mongoose')
const DATABASE_CONECTION = 'mongodb://localhost:27017/test'
mongoose.connect(DATABASE_CONECTION)

const jwt = require('jsonwebtoken')

const md5 = require('md5')

const User = require('./models/user.model')
app.get('/', function(req, res) {
    User.find()
        .select({ _id: true, fullname: true, role: true })
        .exec((err, data) => {
            if (err) return res.json({ error: true, data: err + '' })
            return res.json({ error: false, data })
        })
})

const userRoute = require('./src/routes/user')
const courseRoute = require('./src/routes/course')
const categoryRoute = require('./src/routes/category')
const notificationRoute = require('./src/routes/notification')
const searchRoute = require('./src/routes/search')

app.use(express.static('public'))

app.use('/users', userRoute)
app.use('/courses', courseRoute)
app.use('/categories', categoryRoute)
app.use('/notifications', notificationRoute)
app.use('/search', searchRoute)

app.listen(3000, function() {
    console.log('App listening on port 3000!')
})

websocket.on('connection', socket => {
    console.log('A client just joined on', socket.id)
})
